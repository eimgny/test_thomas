USE MBRSHIP;
GO

DROP TABLE IF EXISTS #TEMP_MBR_LIST;

select distinct
a.member_id
INTO #TEMP_MBR_LIST
from mbrship.dbo.t_membership_trend_dtl a
	left join (
			select 
			a.*
			from (select distinct
						a.member_id,
						EOMONTH(DATEFROMPARTS(year(getdate()), B.MEAS_END_MTH,1),0) AS END_RANGE,
						DATEADD(DAY,1,DATEADD(month,B.MEAS_BEG_AMOUNT,EOMONTH(DATEFROMPARTS(year(getdate()), B.MEAS_END_MTH,1),0))) AS BEG_RANGE,
						C.MyTagName,
						C.DOS
					from mbrship.dbo.t_membership_trend_dtl a
						INNER JOIN MBRSHIP.DBO.T_MEMBER_MAP MAP
							ON A.MEMBER_ID = MAP.MEMBER_ID
						left JOIN ExcelsiorCDR.DBO.ic_patientmytags C
							ON MAP.ClinicID = C.ClinicID
								AND MAP.PatientID = C.PatientID
								and c.MyTagName = 'Mammo'
						left JOIN HEDIS.DBO.T_MEASURE_MYTAGS B
							ON c.MyTagName = b.MyTagName
					where a.PATIENTGENDER = 'F'
						and year(a.patientdob) between 1947 and 1971
						and a.period = '1 year lookback'
					) a
				WHERE CAST(a.DOS AS DATE) BETWEEN BEG_RANGE AND END_RANGE
			) b
		on a.member_id = b.MEMBER_ID
	LEFT join OPENQUERY (MariaDBDev, 
				'SELECT 
				MEMBER_ID,
				MAX(NUMERATOR_HIT) AS NUMERATOR_HIT
				FROM QM.T_MEASURE_MBR_CURR
				WHERE MEASURE_ID = 3
				AND DENOMINATOR = 1
				GROUP BY 
				MEMBER_ID
				') c
		ON A.MEMBER_ID = C.MEMBER_ID
where a.PATIENTGENDER = 'F'
	and year(a.patientdob) between 1947 and 1971
	and a.period = '1 year lookback'
	and b.member_id is null
	AND (C.NUMERATOR_HIT = 0 OR C.NUMERATOR_HIT IS NULL)
;

SELECT
D.DIV_ID as [Division ID],
D.DIV_NAME as [Division Name],
A.MEMBER_ID as [Member ID],
MBR.MBR_FIRST_NAME as [First Name],
MBR.MBR_LAST_NAME as [Last Name],
MBR.MBR_DOB [DOB],
MBR.MBR_GENDER [Gender],
MAX(CASE WHEN E.PHONE_TYPE = 'HOME' THEN CONCAT(LEFT(PHONE,3),'-',RIGHT(LEFT(PHONE,6),3),'-',RIGHT(PHONE,4)) END) AS [Home Phone],
MAX(CASE WHEN E.PHONE_TYPE = 'MOBILE' THEN CONCAT(LEFT(PHONE,3),'-',RIGHT(LEFT(PHONE,6),3),'-',RIGHT(PHONE,4)) END) AS [Mobile Phone],
MAX(CASE WHEN E.PHONE_TYPE = 'WORK' THEN CONCAT(LEFT(PHONE,3),'-',RIGHT(LEFT(PHONE,6),3),'-',RIGHT(PHONE,4)) END) AS [Work Phone]
FROM #TEMP_MBR_LIST A
	INNER JOIN MBRSHIP.DBO.T_MEMBER MBR
		ON A.MEMBER_ID = MBR.MEMBER_ID
	INNER JOIN MBRSHIP.DBO.T_MEMBER_MAP B
		ON A.MEMBER_ID = B.MEMBER_ID
	INNER JOIN MDLAND.DBO.T_CLINIC C
		ON B.ClinicID = C.CLINIC_ID
	INNER JOIN MDLAND.DBO.T_DIVISION D
		ON C.DIV_ID = D.DIV_ID
	INNER JOIN MBRSHIP.DBO.T_MDLAND_MBR_PHONE E
		ON A.MEMBER_ID = E.MEMBER_ID
WHERE D.SPECIALTY_GROUP IN('PRIMARY CARE') 
--and d.[DIV_ID] = ''

GROUP BY 
D.DIV_ID,
D.DIV_NAME,
A.MEMBER_ID,
MBR.MBR_FIRST_NAME,
MBR.MBR_LAST_NAME,
MBR.MBR_DOB,
MBR.MBR_GENDER

ORDER BY [DIVISION ID]
;